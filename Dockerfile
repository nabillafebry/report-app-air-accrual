# Alpine Linux with OpenJDK JRE
FROM openjdk:8-jre-alpine
# copy WAR into image
COPY target/report-air-accrual.jar /report-air-accrual.jar
# run application with this command line[
CMD ["java", "-jar", "/report-air-accrual.jar"]
